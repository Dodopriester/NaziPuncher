
/**
 * Interface TICKER. 
 * Jede Klasse, die dieses Interface implementiert, 
 * muss die Methode tick() ueberschreiben. 
 * Ihr Code wird dann nebenlaeufig zum restlichen Code ausgefuehrt. 
 * Ticker-Objekte werden ueber ein TICKMANGER-Objekt gesteuert.
 * 
 * @author      Mike Ganshorn
 * @version     v0.1 (2016-05-27)
 */

public interface TICKER 
extends ea.Ticker
{
    public abstract void tick();
}
