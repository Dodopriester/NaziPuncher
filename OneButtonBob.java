/*
public class OneButtonBob extends SPIEL{
   
    private RECHTECK R1;
    private RECHTECK R2;
    private FIGUR bob;
    private FIGUR feuer1;
    private int sprungdauer;
    private FIGUR tod1;
    private int level;
    private FIGUR leiter1;
    private String bzustand;
    
    public OneButtonBob(){
        
        bob = new FIGUR(50, 450, "bob.eaf");
        bob.aktivMachen();
        
        R1 = new RECHTECK(385, 50);
        R1.setzeFarbe("weiss");
        R1.setzeMittelpunkt(150, 500);
        R1.passivMachen();
        
        R2 = new RECHTECK(385, 50);
        R2.setzeFarbe("weiss");
        R2.setzeMittelpunkt(600, 500);
        R2.passivMachen();
        
        super.tickerNeuStarten(5);
        
        feuer1 = new FIGUR(375, 490,  "feuer.eaf");
        feuer1.neutralMachen();
        
        sprungdauer = 100;
        
        tod1 = new FIGUR(375, 500, "tod.eaf");
        tod1.setzeSichtbar(false);
        
        level = 1;
        
        leiter1 = new FIGUR(1,1, "leiter.eaf");
        leiter1.neutralMachen();
        leiter1.sichtbarSetzen(false);
        
        bzustand = "gehen";
        
    }
    
    @Override
    public void tick(){
            
        if(bzustand == "gehen"){
            
            bob.verschiebenUm(0.5 , 0);
        
        } else if(bzustand == "leiter"){
            
            bob.verschiebenUm(0, -1);
            
        } else if(bob.nenneM_y() < 350){
            
            bzustand = "gehen";
            bob.aktivMachen();
            
        }
        
        if(bob.beruehrt(tod1) || bob.nenneM_y() > 600){
            
            bob.setzeSichtbar(false);
            
            super.hintergrundgrafikSetzen("gameover.jpg");
            
        } else if(bob.nenneM_x() >= 800){
            
            bob.mittelpunktSetzen(50, 450);
            level++;
            levelmanager2();
            
        }
        
    }
    
    @Override 
    public void tasteReagieren(int tastenkuerzel){
        
        if(tastenkuerzel == 30 && level == 1){
            
            bob.sprung(5);
            
        } else if(tastenkuerzel == 26 && level == 2 &&  bob.beruehrt(leiter1)){
            
            bzustand = "leiter";
            bob.neutralMachen();
            
        } 
        
    }
    
    public void levelmanager2(){
        
        if(level == 2){
            
            leiter1.mittelpunktSetzen(375,425);
            leiter1.sichtbarSetzen(true); 
            
            R1.setzeMittelpunkt(200, 490);
            R1.setzeGroesse(400, 50);
            
            feuer1.mittelpunktSetzen(1, 1);
            feuer1.neutralMachen();
            feuer1.sichtbarSetzen(false);
            
            R2.setzeMittelpunkt(600, 375);
            
        }
        
    }
    
}
*/