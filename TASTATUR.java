import ea.Taste;

/**
 * Klasse TASTE stellt Konstanten zur Verfuegung fuer die Methode tasteReagieren. 
 * Damit muessen die Tasten nicht mehr als Zahl angegeben werden sondern koennen 
 * beim Namen genannt werden.
 * 
 *    Ziffern:              TASTE._0 bis TASTE._9
 * 
 *    Buchstaben:           TASTE.A bis TASTE.B
 * 
 *    Pfeiltasten:          TASTE.RAUF, TASTE.RUNTER, TASTE LINKS, TASTE.RECHTS
 * 
 *    Sondertasten:         TASTE.ENTER, TASTE.ESCAPE, TASTE.LEERTASTE, TASTE.MINUS, TASTE.PLUS
 * 
 *    Ungueltige Tasten:    TASTE.INVALID
 * 
 * @author      mike ganshorn
 * @version     1.0 (2015-12-01)
 */
public class TASTATUR 
extends Taste
{
    
}
