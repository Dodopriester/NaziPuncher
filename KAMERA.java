import ea.edu.FensterE;
import ea.Kamera;
import ea.BoundingRechteck;
import ea.Raum;
import ea.Vektor;

/**
 * Klasse KAMERA liefert von der unendlich grossen Zeichenebene einen rechteckigen Ausschnitt. 
 * Dieser kann im (viel groesseren) Spielfeld verschoben werden wie der Aufnahmebereiche einer 
 * schwenkenden Kamera. Man sieht also immer nur einen begrenzten rechteckigen Ausschnitt der Welt.
 * 
 * @author      mike ganshorn
 * @version     v1.0 (2016-01-25)
 */
public class KAMERA
{
    private Kamera kamera;

    
    /**
     * Konstruktor der Klasse KAMERA
     */
    public KAMERA()
    {
        this.kamera = FensterE.getFenster().cam;
    }

    
    public void setzeBounds(int links, int oben, int rechts, int unten)
    {
        this.kamera.boundsSetzen( new BoundingRechteck(links, oben, rechts, unten) );
    }
    
    
    public void loescheFokus()
    {
        this.kamera.fokusLoeschen();
    }
    
    
    public void setzeFokus(Raum r)
    {
        this.kamera.fokusSetzen(r);
    }
    
    
    public void setzeFokusVerzug(int x_verzug, int y_verzug)
    {
        this.kamera.fokusVerzugSetzen( new Vektor(x_verzug, y_verzug) );
    }
    
    
    public void setzeZentrum(int x, int y)
    {
        this.kamera.zentrumSetzen(x, y);
    }
    
    
    public void verschieben(int deltaX, int deltaY)
    {
        this.kamera.verschieben(deltaX, deltaY);
    }
    
}
