import ea.Knoten;
import ea.edu.BildE;
import ea.edu.FensterE;
import ea.Raum;
import ea.Punkt;

/**
 * Klasse WECHSELBILD kann mehrere Bilder verwalten von denen immer nur eines angezeigt wird.
 * 
 * @author      mike ganshorn
 * @version     v1.1 (2016-02-17)
 * 
 * @changelog   1.1 verschiebenUm greift nun konsequent auf bewegen zurueck
 *                  sichtbar(int) hinzugefuegt
 *                  Methoden in allen Klassen vereinheitlicht (bis auf indiv. Methoden)
 * 
 *              1.0 Grundlegende Implementierung
 * 
 *              
 */
public class WECHSELBILD
extends Knoten
{
    private int anz_Bilder;
    private BildE[] bilder;
    private int akt_BildNr;

    
    /**
     * Konstruktor der Klasse WECHSELBILD
     */
    public WECHSELBILD(int x, int y, String... dateien)
    {
        this.anz_Bilder = dateien.length;
        this.bilder = new BildE[this.anz_Bilder];

        for ( int i=0 ; i<this.anz_Bilder ; i++ )
        {
            this.bilder[i] = new BildE( x , y , dateien[i] );
            this.bilder[i].sichtbarSetzen(false);
            this.add(this.bilder[i]);
        }
        this.setzeMittelpunkt(x, y);
        

        this.bilder[0].sichtbarSetzen(true);
        this.akt_BildNr = 0;

        FensterE.getFenster().wurzel.add(this);
    }

    
    /**
     * Macht das aktuelle Bild unsichtbar und das naechste Bild sichtbar. 
     * Am Ende der Bilder wird wieder von Vorne bekonnen.
     *
     */
    public void wechseln()
    {
        this.bilder[this.akt_BildNr].sichtbarSetzen(false);
        this.akt_BildNr = (this.akt_BildNr + 1) % this.anz_Bilder;
        this.bilder[this.akt_BildNr].sichtbarSetzen(true);
    }
    

    /**
     * Macht das aktuelle Bild unsichtbar und das genannte Bild sichtbar. 
     * Sollte die gewahlte Bildnummer zu gross sein, wird ein anderes Bild sichtbar gemacht.
     *
     * @param   bildNr  Die Nummer des Bildes, das sichtbar gemacht werden soll
     */
    public void wechseln(int bildNr)
    {
        this.bilder[this.akt_BildNr].sichtbarSetzen(false);
        this.akt_BildNr = bildNr % this.anz_Bilder;
        this.bilder[this.akt_BildNr].sichtbarSetzen(true);
    }

    
    /**
     * Aktiviert bei diesem Objekt die Jump'n'Run-Physik und macht es aktiv. 
     * Es wir nun von passiven Objekten behindert / aufgehalten und 
     * unterliegt der Schwerkraft.
     *
     */
    public void aktivMachen()
    {
        for ( BildE b: this.bilder )
        {
            b.aktivMachen();
        }
//         super.aktivMachen();
    }

    
    /**
     * Berechnet den Abstand von Mittelpunkt zu Mittelpunkt zu einem anderen Objekt in Pixeln. 
     *
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  x-Abstand der Mittelpunkte in Pixeln. 
     *          '>0' bedeutet, dass dieses Objekt rechts vom anderen liegt. 
     */
    public int berechneAbstandX(Raum grafikObjekt)
    {
        return this.bilder[this.akt_BildNr].mittelPunkt().x() - grafikObjekt.mittelPunkt().x();
    }

    
    /**
     * Berechnet den Abstand von Mittelpunkt zu Mittelpunkt zu einem anderen Objekt in Pixeln. 
     *
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  y-Abstand der Mittelpunkte in Pixeln. 
     *          '>0' bedeutet, dass dieses Objekt unter dem anderen liegt. 
     */
    public int berechneAbstandY(Raum grafikObjekt)
    {
        return this.bilder[this.akt_BildNr].mittelPunkt().y() - grafikObjekt.mittelPunkt().y();
    }

    
    /**
     * Prueft, ob dieses Grafik-Objekt ein anderes beruehrt. 
     *
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Der Rückgabewert
     */
    public boolean beruehrt(Raum grafikObjekt)
    {
        return this.bilder[this.akt_BildNr].schneidet(grafikObjekt);
//         return grafikObjekt.schneidet(this);
    }
    
    
    /**
     * Prueft, ob dieses Grafik-Objekt einen Punkt beruehrt. 
     *
     * @param   p   Der Punkt
     * @return      'true', wenn der Punkt beruehrt wird, sonst 'false'
     */
    public boolean beruehrt(Punkt p)
    {
        return this.bilder[this.akt_BildNr].beinhaltet(p);
//         return super.beinhaltet(p);
    }
    
    
    /**
     * Methode beinhaltetPunkt
     *
     * @param   x   x-Koordinate des Punkts (Pixel)
     * @param   y   x-Koordinate des Punkts (Pixel)
     * @return      true, wenn Punkt innerhalb der Grafik
     */
    public boolean beinhaltetPunkt(int x, int y) 
    {
        return this.bilder[this.akt_BildNr].beinhaltet( new Punkt(x, y) );
//         return super.beinhaltet( new Punkt(x, y) );
    }

    
    /**
     * Nennt die x-Koordinate des Mittelpunkts dieses Grafik-Objekts. 
     *
     * @return  x-Koordinate des Mittelpunkts dieses Grafik-Objekts
     */
    public int nenneM_x()
    {
        return this.bilder[this.akt_BildNr].mittelPunkt().x();
//         return this.mittelPunkt().x();
    }

    
    /**
     * Nennt die y-Koordinate des Mittelpunkts dieses Grafik-Objekts. 
     *
     * @return  y-Koordinate des Mittelpunkts dieses Grafik-Objekts
     */
    public int nenneM_y()
    {
        return this.bilder[this.akt_BildNr].mittelPunkt().y();
//         return this.mittelPunkt().y();
    }

    
    /**
     * Gibt an, ob dieses Grafik-Objekt im Moment sichtbar ist. 
     *
     * @return  'true', wenn sichtbar; 'false', wenn unsichtbar
     */
    public boolean nenneSichtbar()
    {
        return this.bilder[this.akt_BildNr].sichtbar();
    }

    
    /**
     * Entfernt die Jump'n'Run-Physik von diesem Grafik-Objekt.
     *
     */
    public void neutralMachen()
    {
        for ( BildE b: this.bilder )
        {
            b.neutralMachen();
        }
//         super.neutralMachen();
    }

    
    /**
     * Aktiviert bei diesem Objekt die Jump'n'Run-Physik und macht es passiv. 
     * Es kann nun aktive Objekte behindern / aufhalten.  
     * Dieses Objekt unterliegt NICHT der Schwerkraft.
     *
     */
    public void passivMachen()
    {
        for ( BildE b: this.bilder )
        {
            b.passivMachen();
        }
//         super.passivMachen();
    }

    
    /**
     * Schaltet die Schwerkraft fuer ein AKTIVES Objekt ein oder aus.
     *
     * @param   b   'true' = Schwerkraft an, 'false' = Schwerkraft aus
     */
    public void schwerkraftAktivSetzen(boolean b)
    {
        for ( BildE be: this.bilder )
        {
            be.schwerkraftAktivSetzen(b);
        }
//         super.schwerkraftAktivSetzen(b);
    }

    
    /**
     * Setzt die Staerke der Schwerkraft. Standard-wert 4. 
     * Groessere Werte = langsamers Fallen, kleinere Werte = schnelleres Fallen.
     *
     * @param   n   ganze Zahl von 1-10
     */
    public void schwerkraftSetzen(int n)
    {
        for ( BildE bild: this.bilder )
        {
            bild.schwerkraftSetzen(n);
        }
//         super.schwerkraftSetzen(n);
    }

    
    /**
     * Setzt den Mittelpunkt neu.
     *
     * @param   x   neue x-Koordinate
     * @param   y   neue y-Koordinate
     */
    public void setzeMittelpunkt(int x, int y)
    {
//         super.mittelpunktSetzen( x , y );
        for ( BildE bild: this.bilder )
        {
            bild.mittelpunktSetzen(x, y);
        }
    }

    
    /**
     * Setzt das Objekt sichtbar / unsichtbar.
     *
     * @param   b   'true' = sichtbar, 'false' = unsichtbar
     */
    public void setzeSichtbar(boolean b)
    {
        this.bilder[this.akt_BildNr].sichtbarSetzen(b);
    }

    
    /**
     * Verschiebt das Objekt auf dem Grafikfenster.
     *
     * @param   deltaX  Anzahl an Pixeln, die in x-Richtung verschoben wird ('+' = rechts)
     * @param   deltaY  Anzahl an Pixeln, die in y-Richtung verschoben wird ('+' = runter)
     */
    public void verschiebenUm(int deltaX, int deltaY)
    {
//         super.bewegen( deltaX , deltaY );
        for (BildE bild: this.bilder)
        {
            bild.bewegen(deltaX, deltaY);
        }
    }
    
    
    /**
     * Gibt an, ob dieses (aktive) Grafik-Objekt auf irgend einem anderen (passiven) Grafik-Objekt steht. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist!
     *
     * @return  'true', wenn dieses Objekt auf irgend einem anderen steht; 'false', wenn nicht
     */
    public boolean steht()
    {
        return this.bilder[this.akt_BildNr].steht();
//         return super.steht();
    }
    
    
    /**
     * Gibt an, ob dieses (aktive) Grafik-Objekt auf einem bestimmten anderen (passiven) Grafik-Objekt steht. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist und das andere passiv!
     *
     * @return  'true', wenn dieses Objekt auf dem anderen steht; 'false', wenn nicht
     */
    public boolean stehtAuf(Raum grafikObjekt)
    {
        return this.bilder[this.akt_BildNr].stehtAuf(grafikObjekt);
        
//         return this.stehtAuf(grafikObjekt);

//         boolean steht_drauf = false;
//         for ( int z=0 ; z<this.bilder.length ; z++ )
//         {
//             if ( this.bilder[z].stehtAuf(grafikObjekt) )
//             {
//                 System.out.println(""+z+" steht");
//                 steht_drauf = true;
//             }
//         }
//         return steht_drauf;
    }
    
    
    /**
     * Laesst das (aktive) Grafik-Objekt von einem (passiven) Grafik-Objekt abspringen. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist. 
     * Funktioniert NUR DANN, wenn dieses (aktive) Objekt auf einem passiven Objekt steht.
     *
     * @param   sprungStaerke   1 = kleiner Hopser; ... 10 = hoher Sprung
     */
    public boolean sprung(int sprungStaerke)
    {
        boolean gesprungen = false;
        for ( BildE b: this.bilder )
        {
            gesprungen |= b.sprung( sprungStaerke );
        }
        return gesprungen;
//         return super.sprung(sprungStaerke);
    }
    
    
    /**
     * Setzt das Bild mit dem Index i sichtbar, alle anderen unsichtbar.
     *
     * @param   i   Der Index des bildes, das sichtbar gesetzt werden soll
     */
    public void sichtbar(int i)
    {
        for ( int z=0 ; z<this.bilder.length ; z++ )
        {
            if ( z == i )
            {
                this.bilder[z].sichtbarSetzen(true);
            }
            else
            {
                this.bilder[z].sichtbarSetzen(false);
            }
        }
    }
    
    
    /**
     * liefert den Sinus des Drehwinkels der Grafik
     *
     * @return  Sinus des aktuellen Drehwinkels
     */
    public double sin_Drehwinkel()
    {
        return Math.sin( super.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * liefert den Cosinus des Drehwinkels der Grafik
     *
     * @return  Cosinus des aktuellen Drehwinkels
     */
    public double cos_Drehwinkel()
    {
        return Math.cos( super.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * Dreht die Grafik um einen Winkel
     *
     * @param   winkelAenderung     +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void drehenUm(int winkelAenderung)
    {
        super.drehenRelativ( (double)( -winkelAenderung ) );
    }
    
    
    /**
     * Setzt den Drehwinkel auf eine absoluten neuen Wert
     *
     * @param   neuerDrehwinkel     der neue Drehwinkel
     *                              +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void setzeDrehwinkel(int neuerDrehwinkel)
    {
        super.drehenAbsolut( (double)( -neuerDrehwinkel ) );
    }
    
    
    /**
     * Nennt den Winkel, um den die Grafik gedreht wurde
     *
     * @return      der Winkel, um den die Grafik gedreht wurde
     *              0: wenn nicht gedreht
     *              +: wenn mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *              -: wenn mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public int nenneWinkel()
    {
        return (int)( -this.gibDrehung() );
    }
}
