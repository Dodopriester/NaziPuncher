/** @author     mike ganshorn
 * 
 *  @Version    2.4 (2016-02-17)
 *  
 *  @changelog  2.4 WECHSELBILD erbt von Knoten und damit von Raum
 *                  verschiebenUm greift auf bewegen zurueck
 *                  Methoden in allen Klassen vereinheitlicht (bis auf indiv. Methoden)
 *  
 *              2.3 Methode beruehrt(WECHSELBILD) hinzugefuegt
 *  
 *              2.2 Jump'n'Run-Physik hinzu gefuegt.
 *  
 *              2.1 keine Abhaengigkeit mehr zwischen den alpha-Formen
 *                  Der Mittelpunkt des Dreiecks ist hier der Mittelpunkt des umhuellenden Rechtecks !!!
 * 
 *              
*/

import ea.edu.DreieckE;
import ea.Raum;

/**
 * Diese Klasse stellt ein einfaches Dreieck dar.
 */
public class DREIECK 
        extends DreieckE 
{
    
    /**
     * Die Farbe dieses Dreiecks
     */
    private String farbe;
    
    /**
     * Gibt an, ob dieses Dreieck sichtbar ist.
     */
    private boolean sichtbar;
    
    /**
     * x-Koordinate des Eckpunkts A
     */
    private int A_x;
    
    /**
     * y-Koordinate des Eckpunkts A
     */
    private int A_y;
    
    /**
     * x-Koordinate des Eckpunkts B
     */
    private int B_x;
    
    /**
     * y-Koordinate des Eckpunkts B
     */
    private int B_y;
    
    /**
     * x-Koordinate des Eckpunkts C
     */
    private int C_x;
    
    /**
     * y-Koordinate des Eckpunkts C
     */
    private int C_y;
    
     /**
     * x-Koordinate des Mittelpunkts des umschliessenden Rechtecks
     */
    private int M_x;
    
    /**
     * y-Koordinate des Mittelpunkts des umschliessenden Rechtecks
     */
    private int M_y;
    
    
    /**
     * Konstruktor der Klasse <code>DREIECK</code>. Erstellt ein neues Standard-Dreieck.
     */
    public DREIECK() 
    {
        this( 80 , 190 , 150 , 70 , 220 , 190 );
    }
    
    
    /**
     * Konstruktor der Klasse <code>DREIECK</code>. Erstellt ein neues Dreieck mit gegebenen Eck-Koordinaten. 
     * Die Reihenfolge der Ecken ist egal. 
     *
     * @param   x1  Die x-Koordinate der ersten Ecke
     * @param   y1  Die y-Koordinate der ersten Ecke
     * @param   x2  Die x-Koordinate der zweiten Ecke
     * @param   y2  Die y-Koordinate der zeiten Ecke
     * @param   x3  Die x-Koordinate der dritten Ecke
     * @param   y3  Die y-Koordinate der dritten Ecke
     */
    public DREIECK(int x1, int y1, int x2, int y2, int x3, int y3) 
    {
        this.A_x = x1;
        this.A_y = y1;
        this.B_x = x2;
        this.B_y = y2;
        this.C_x = x3;
        this.C_y = 50;
        this.M_x = ( Math.min(Math.min(x1,x2),x3) + Math.max(Math.max(x1,x2),x3) ) / 2 ;
        this.M_y = ( Math.min(Math.min(y1,y2),y3) + Math.max(Math.max(y1,y2),y3) ) / 2 ;
        super.punkteSetzen(x1, y1, x2, y2, x3, y3);
        this.sichtbar = true;
        super.sichtbarSetzen(true);
        this.farbe = "Gruen";
        super.farbeSetzen(this.farbe);
    }
    
    
    /**
     * Setzt die Ecken dieses Dreiecks (A, B, C) neu.
     * @param   a_x Die X-Koordinate des Punktes A
     * @param   a_y Die Y-Koordinate des Punktes A
     * @param   b_x Die X-Koordinate des Punktes B
     * @param   b_y Die Y-Koordinate des Punktes B
     * @param   c_x Die X-Koordinate des Punktes C
     * @param   c_y Die Y-Koordinate des Punktes C
     */
    public void setzeEcken(int a_x, int a_y, int b_x, int b_y, int c_x, int c_y) 
    {
        this.A_x = a_x;
        this.A_y = a_y;
        this.B_x = b_x;
        this.B_y = b_y;
        this.C_x = c_x;
        this.C_y = c_y;
        this.M_x = ( Math.min(Math.min(a_x,b_x),c_x) + Math.max(Math.max(a_x,b_x),c_x) ) / 2 ;
        this.M_y = ( Math.min(Math.min(a_y,b_y),c_y) + Math.max(Math.max(a_y,b_y),c_y) ) / 2 ;
        super.punkteSetzen(a_x, a_y, b_x, b_y, c_x, c_y);
    }
    
    
    /**
     * Setzt die Farbe dieses Dreiecks neu.
     * @param   farbeNeu    Diese Farbe erhaelt das Dreieck (z.B. "Rot")
     */
    public void setzeFarbe(String farbeNeu) 
    {
        this.farbe = farbeNeu;
        super.farbeSetzen(farbe);
    }
    
    
    /**
     * Setzt den Mittelpunkt dieses Dreieck neu.<br />
     * <b>ACHTUNG!</b><br />
     * Dies ist <i>nicht</i> der geometrische Mittelpunkt. Denkt man sich ein Rechteck, 
     * das man "genau ueber das Dreieck" spannt, sodass es dieses gerade von allen Seiten umschliesst, 
     * so ist der Mittelpunkt <b>dieses Rechtecks</b> der, der hier neu gesetzt wird.
     * @param   m_x Die X-Koordinate des neuen Mittelpunktes
     * @param   m_y Die Y-Koordinate des neuen Mittelpunktes
     */
    public void setzeMittelpunkt(int m_x, int m_y) 
    {
        int deltaX = m_x - this.M_x;
        int deltaY = m_y - this.M_y;
        this.A_x = this.A_x + deltaX;
        this.A_y = this.A_y + deltaY;
        this.B_x = this.B_x + deltaX;
        this.B_y = this.B_y + deltaY;
        this.C_x = this.C_x + deltaX;
        this.C_y = this.C_y + deltaY;
        this.M_x = m_x;
        this.M_y = m_y;
        super.mittelpunktSetzen(m_x, m_y);
    }
    
    
    /**
     * Setzt, ob dieses Dreieck sichtbar sein soll.
     * @param   sichtbarNeu Ist dieser Wert <code>true</code>, ist nach dem Aufruf dieser Methode dieses Dreieck 
     * sichtbar. Ist dieser Wert <code>false</code>, so ist nach dem Aufruf dieser Methode dieses Dreieck unsichtbar.
     */
    public void setzeSichtbar(boolean sichtbarNeu) 
    {
        this.sichtbar = sichtbarNeu;
        super.sichtbarSetzen(sichtbarNeu);
    }
    
    
    /**
     * Verschiebt dieses Dreieck um eine Verschiebung - angegeben durch ein "Delta X" und "Delta Y".
     * @param   deltaX  Der X Anteil dieser Verschiebung. Positive Werte verschieben nach rechts, negative nach links.
     * @param   deltaY  Der Y Anteil dieser Verschiebung. Positive Werte verschieben nach unten, negative nach oben.
     */
    public void verschiebenUm(int deltaX, int deltaY) 
    {
        this.A_x = this.A_x + deltaX;
        this.A_y = this.A_y + deltaY;
        this.B_x = this.B_x + deltaX;
        this.B_y = this.B_y + deltaY;
        this.C_x = this.C_x + deltaX;
        this.C_y = this.C_y + deltaY;
        this.M_x = this.M_x + deltaX;
        this.M_y = this.M_y + deltaY;
        super.bewegen(deltaX, deltaY);
    }
    
    
    /**
     * Methode beruehrt
     *
     * @param   r   Ein anderes BILD, RECHTECK, KREIS, DREIECK, ...
     * @return  true, wenn sich die beiden Objekte ueberschneiden
     */
    public boolean beruehrt(Raum r) 
    {
        return super.schneidet(r);
    }
    
    
    /**
     * Prueft, ob dieses Grafik-Objekt einen Punkt beruehrt. 
     *
     * @param   p   Der Punkt
     * @return      'true', wenn der Punkt beruehrt wird, sonst 'false'
     */
    public boolean beruehrt(ea.Punkt p)
    {
        return super.beinhaltet(p);
    }
    
    
    /**
     * Methode beinhaltetPunkt
     *
     * @param   x   x-Koordinate des Punkts (Pixel)
     * @param   y   x-Koordinate des Punkts (Pixel)
     * @return      true, wenn Punkt innerhalb der Grafik
     */
    public boolean beinhaltetPunkt(int x, int y) 
    {
        return super.beinhaltet( new ea.Punkt(x, y) );
    }
    
    
    /**
     * Diese Methode gibt die x-Koordinate des Mittelpunkts dieses Dreiecks zurueck
     * @return  Die aktuelle x-Koordinate des Mittelpunktes dieses Dreiecks
     */
    public int nenneM_x()
    {
        return this.M_x;
    }
    
    
    /**
     * Diese Methode gibt die y-Koordinate des Mittelpunkts dieses Kreises zurueck
     * @return  Die aktuelle y-Koordinate des Mittelpunktes dieses Kreises
     */
    public int nenneM_y()
    {
        return this.M_y;
    }
    
    
    /**
     * Diese Methode gibt die Farbe dieses Dreiecks zurueck
     * @return  Die aktuelle Farbe dieses Dreiecks
     */
    public String nenneFarbe()
    {
        return this.farbe;
    }
    
    
    /**
     * Diese Methode gibt die Sichtbarkeit dieses Dreiecks zurueck
     * @return  Die aktuelle Sichtbarkeit dieses Dreiecks
     */
    public boolean nenneSichtbar()
    {
        return this.sichtbar;
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Rechtecks vom Mittelpunkt 
     * eines anderen Grfik-Objekts in x-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Rechtecks vom anderen Grafik-Objekt in x-Richtung (>0, wenn dieses Rechteck rechts des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandX(Raum grafikObjekt)
    {
        return this.M_x - grafikObjekt.mittelPunkt().x();
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Dreiecks vom Mittelpunkt 
     * eines anderen Grfik-Objekts in y-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Dreiecks vom anderen Grafik-Objekt in y-Richtung (>0, wenn dieses Dreieck unterhalb des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandY(Raum grafikObjekt)
    {
        return this.M_y - grafikObjekt.mittelPunkt().y();
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun aktiv und kann passive Objekte NICHT durchdringen.
     */
    @Override
    public void aktivMachen()
    {
        super.aktivMachen();
    }
    
    
    /**
     * Methode zum Deaktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     */
    @Override
    public void neutralMachen()
    {
        super.neutralMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void passivMachen()
    {
        super.passivMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Schwerkraft fuer dieses Objekt. 
     * Von nun an unterliegt das Objekt der Schwerkraft und faellt runter.
     * 
     * @param   b   'true', wenn die Schwerkraft wirken soll, 'false', wenn nicht.
     */
    @Override
    public void schwerkraftAktivSetzen(boolean b)
    {
        super.schwerkraftAktivSetzen(b);
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void schwerkraftSetzen(int n)
    {
        super.schwerkraftSetzen(n);
    }
    
    
    /**
     * Methode zum hochspringen, wenn das Objekt aktiv ist und auf einem passiven Objekt steht.
     *
     * @param   staerke     1 = schwach ... 10 = stark
     * @return  Der Rückgabewert
     */
    @Override
    public boolean sprung(int staerke)
    {
        return super.sprung(staerke);
    }
    
    
    /**
     * Prueft ob dieses Grafik-Objekt, wenn es aktiv ist, auf einem anderen, passiven Grafik-Objekt steht.
     *
     * @param   r   Das andere Grafik-Objekt
     * @return  'true', wenn dieses Grafik-Objekt aktiv gesetzt ist und auf einem passiven Grafik-Objekt steht
     */
    @Override
    public boolean stehtAuf(Raum r)
    {
        return super.stehtAuf(r);
    }
    
    
    /**
     * Gibt an, ob dieses (aktive) Grafik-Objekt auf irgend einem anderen (passiven) Grafik-Objekt steht. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist!
     *
     * @return  'true', wenn dieses Objekt auf irgend einem anderen steht; 'false', wenn nicht
     */
    public boolean steht()
    {
        return super.steht();
    }
    
    
    /**
     * liefert den Sinus des Drehwinkels der Grafik
     *
     * @return  Sinus des aktuellen Drehwinkels
     */
    public double sin_Drehwinkel()
    {
        return Math.sin( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * liefert den Cosinus des Drehwinkels der Grafik
     *
     * @return  Cosinus des aktuellen Drehwinkels
     */
    public double cos_Drehwinkel()
    {
        return Math.cos( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * Dreht die Grafik um einen Winkel
     *
     * @param   winkelAenderung     +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void drehenUm(int winkelAenderung)
    {
        this.drehenRelativ( (double)( -winkelAenderung ) );
    }
    
    
    /**
     * Setzt den Drehwinkel auf eine absoluten neuen Wert
     *
     * @param   neuerDrehwinkel     der neue Drehwinkel
     *                              +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void setzeDrehwinkel(int neuerDrehwinkel)
    {
        this.drehenAbsolut( (double)( -neuerDrehwinkel ) );
    }
    
    
    /**
     * Nennt den Winkel, um den die Grafik gedreht wurde
     *
     * @return      der Winkel, um den die Grafik gedreht wurde
     *              0: wenn nicht gedreht
     *              +: wenn mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *              -: wenn mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public int nenneWinkel()
    {
        return (int)( -this.gibDrehung() );
    }
    
    
//     /**
//      * Prueft, ob ein WECHSELBILD-Objekt beruehrt wird.
//      *
//      * @param   wb  Das WECHSELBILD-Objekt
//      * @return  'true', wenn ein anderes WECHSELBILD-Objekt beruehrt wird
//      */
//     public boolean beruehrt(WECHSELBILD wb)
//     {
//         return wb.beruehrt(this);
//     }
    
}