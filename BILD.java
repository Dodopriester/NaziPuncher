import ea.edu.*;
import ea.*;

/**
 * Klasse BILD zum Darstellen eines GIF, JPG, PNG in EDU-Engine
 * 
 * @author      mike ganshorn
 * @version     1.4 (2016-02-17)
 * 
 * @changelog   1.4 WECHSELBILD erbt von Knoten und damit von Raum
 *                  verschiebenUm greift auf bewegen zurueck
 *                  Methoden in allen Klassen vereinheitlicht (bis auf indiv. Methoden)
 *  
 *              1.3 Methode beruehrt(WECHSELBILD) hinzugefuegt
 * 
 *              1.2 Jump'n'Run-Physik hbinzu gefuegt
 * 
 *              1.1 Konstruktor setzt nun Bild-Mittelpunkt auf uebergebenen Mittelpunkt (nicht Ecke links oben)
 *              
 *              
 */
public class BILD
extends BildE
{
    
    
    /**
     * BILD Konstruktor
     *
     * @param   x       x-Koordinate im Fenster (Pixel)
     * @param   y       y-Koordinate im Fenster (Pixel)
     * @param   name    Name der Grafik-Datei (im Projekt-Ordner)
     */
    public BILD(int x, int y, String name)
    {
        super(x, y, name);
        this.setzeMittelpunkt(x, y);
    }
    
    
    /**
     * Methode verschiebenUm
     *
     * @param   deltaX  Pixel in x-Richtung (wird bei Bedarf auf ganze Pixel gerundet)
     * @param   deltaY  Pixel in y-Richtung (wird bei Bedarf auf ganze Pixel gerundet)
     */
    public void verschiebenUm(double deltaX, double deltaY)
    {
        super.bewegen( (int)( Math.round(deltaX) ), (int)( Math.round(deltaY) ) );
    }
    
    
    /**
     * Methode beinhaltetPunkt
     *
     * @param   x   x-Koordinate des Punkts (Pixel)
     * @param   y   x-Koordinate des Punkts (Pixel)
     * @return      true, wenn Punkt innerhalb der Grafik
     */
    public boolean beinhaltetPunkt(int x, int y)
    {
        return super.beinhaltet( new Punkt(x, y) );
    }
    
    
    /**
     * Methode setzeMittelpunkt
     *
     * @param   x   x-Koordinate des Mittelpunkts (Pixel)
     * @param   y   y-Koordinate des Mittelpunkts (Pixel)
     */
    public void setzeMittelpunkt(int x, int y)
    {
        super.mittelpunktSetzen(x, y);
    }
    
    
    /**
     * Methode nenneM_x
     *
     * @return  x-Koordinate des Mittelpunkts (Pixel)
     */
    public int nenneM_x()
    {
        return super.zentrum().x();
    }
    
    
    /**
     * Methode nenneM_y
     *
     * @return  y-Koordinate des Mittelpunkts (Pixel)
     */
    public int nenneM_y()
    {
        return super.zentrum().y();
    }
    
    
    /**
     * Methode setzeSichtbar
     *
     * @param   sichtbarNeu     true, wenn die Grafik sichtbar sein soll
     */
    public void setzeSichtbar(boolean sichtbarNeu)
    {
        super.sichtbarSetzen(sichtbarNeu);
    }
    
    
    /**
     * Methode nenneSichtbar
     *
     * @return  true, wenn die Grafik gerade sichbar ist
     */
    public boolean nenneSichtbar()
    {
        return super.sichtbar();
    }
    
    
    /**
     * Methode beruehrt
     *
     * @param   r   Ein anderes BILD, RECHTECK, KREIS, DREIECK, ...
     * @return  true, wenn sich die beiden Objekte ueberschneiden
     */
    public boolean beruehrt(Raum r) {
        return super.schneidet(r);
    }
    
    
    /**
     * Prueft, ob dieses Grafik-Objekt einen Punkt beruehrt. 
     *
     * @param   p   Der Punkt
     * @return      'true', wenn der Punkt beruehrt wird, sonst 'false'
     */
    public boolean beruehrt(Punkt p)
    {
        return super.beinhaltet(p);
    }
    
    
    /**
     * Dreht die Grafik um einen Winkel
     *
     * @param   winkelAenderung     +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void drehenUm(int winkelAenderung)
    {
        this.drehenRelativ( (double)( -winkelAenderung ) );
    }
    
    
    /**
     * Setzt den Drehwinkel auf eine absoluten neuen Wert
     *
     * @param   neuerDrehwinkel     der neue Drehwinkel
     *                              +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void setzeDrehwinkel(int neuerDrehwinkel)
    {
        this.drehenAbsolut( (double)( -neuerDrehwinkel ) );
    }
    
    
    /**
     * Nennt den Winkel, um den die Grafik gedreht wurde
     *
     * @return      der Winkel, um den die Grafik gedreht wurde
     *              0: wenn nicht gedreht
     *              +: wenn mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *              -: wenn mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public int nenneWinkel()
    {
        return (int)( -this.gibDrehung() );
    }
    
    
    /**
     * liefert den Sinus des Drehwinkels der Grafik
     *
     * @return  Sinus des aktuellen Drehwinkels
     */
    public double sin_Drehwinkel()
    {
        return Math.sin( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * liefert den Cosinus des Drehwinkels der Grafik
     *
     * @return  Cosinus des aktuellen Drehwinkels
     */
    public double cos_Drehwinkel()
    {
        return Math.cos( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun aktiv und kann passive Objekte NICHT durchdringen.
     */
    @Override
    public void aktivMachen()
    {
        super.aktivMachen();
    }
    
    
    /**
     * Methode zum Deaktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     */
    @Override
    public void neutralMachen()
    {
        super.neutralMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void passivMachen()
    {
        super.passivMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Schwerkraft fuer dieses Objekt. 
     * Von nun an unterliegt das Objekt der Schwerkraft und faellt runter.
     * 
     * @param   b   'true', wenn die Schwerkraft wirken soll, 'false', wenn nicht.
     */
    @Override
    public void schwerkraftAktivSetzen(boolean b)
    {
        super.schwerkraftAktivSetzen(b);
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void schwerkraftSetzen(int n)
    {
        super.schwerkraftSetzen(n);
    }
    
    
    /**
     * Methode zum hochspringen, wenn das Objekt aktiv ist und auf einem passiven Objekt steht.
     *
     * @param   staerke     1 = schwach ... 10 = stark
     * @return  Der Rückgabewert
     */
    @Override
    public boolean sprung(int staerke)
    {
        return super.sprung(staerke);
    }
    
    
    /**
     * Prueft ob dieses Grafik-Objekt, wenn es aktiv ist, auf einem anderen, passiven Grafik-Objekt steht.
     *
     * @param   r   Das andere Grafik-Objekt
     * @return  'true', wenn dieses Grafik-Objekt aktiv gesetzt ist und auf einem passiven Grafik-Objekt steht
     */
    @Override
    public boolean stehtAuf(Raum r)
    {
        return super.stehtAuf(r);
    }
    
    
    /**
     * Gibt an, ob dieses (aktive) Grafik-Objekt auf irgend einem anderen (passiven) Grafik-Objekt steht. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist!
     *
     * @return  'true', wenn dieses Objekt auf irgend einem anderen steht; 'false', wenn nicht
     */
    public boolean steht()
    {
        return super.steht();
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Rechtecks vom Mittelpunkt 
     * eines anderen Grfik-Objekts in x-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Rechtecks vom anderen Grafik-Objekt in x-Richtung (>0, wenn dieses Rechteck rechts des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandX(Raum grafikObjekt)
    {
        return this.mittelPunkt().x() - grafikObjekt.mittelPunkt().x();
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Kreises vom Mittelpunkt 
     * eines anderen Grfik-Objekts in y-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Kreises vom anderen Grafik-Objekt in y-Richtung (>0, wenn dieser Kreis unterhalb des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandY(Raum grafikObjekt)
    {
        return this.mittelPunkt().x() - grafikObjekt.mittelPunkt().y();
    }
    
    
//     /**
//      * Prueft, ob ein WECHSELBILD-Objekt beruehrt wird.
//      *
//      * @param   wb  Das WECHSELBILD-Objekt
//      * @return  'true', wenn ein anderes WECHSELBILD-Objekt beruehrt wird
//      */
//     public boolean beruehrt(WECHSELBILD wb)
//     {
//         return wb.beruehrt(this);
//     }

}
