//import ea.edu.FensterE;
import ea.AnimationsManager;
import ea.Raum;
import ea.Punkt;

/**
 * Klasse ANIMATIONSMANAGER stellt Methoden zur Verfuegung, mit denen man jedes 
 * Grafik-Objekt animieren kann. 
 * 
 * <b>Er darf erst erzeugt werden, nachdem ein Grafik-Fenster existiert.<b> 
 * Das ist der Fall, wenn vorher entweder das Fenster selbst oder ein 
 * beliebiges Grafik-Objekt erzeugt wurde.
 * 
 * @author      mike ganshorn
 * @version     1.0 (2015-12-01)
 */
public class ANIMATIONSMANAGER
{
    private static AnimationsManager am;
    
    
    /**
     * Konstruktor der Klasse ANIMATIONSMANAGER
     */
    public ANIMATIONSMANAGER()
    {
        //this.am = FensterE.getFenster().animationsManager.getAnimationsManager();
        am = AnimationsManager.getAnimationsManager();
    }
    
    
    /**
     * Methode kreisAnimation - ultra-primitiv-Variante 
     * Animations-Mittelpunkt, Radius, Geschwindigkeit, Drehrichtung sind vorgegeben
     *
     * @param   r   Das zu animierende Grafik-Objekt
     */
    public void kreisAnimation(Raum r)
    {
        am.kreisAnimation(r);
    }
    
    
    /**
     * Methode kreisAnimation - primitiv-Variante 
     * Geschwindigkeit, Drehrichtung sind vorgegeben
     *
     * @param   r   Das zu animierende Grafik-Objekt
     * @param   x   x-Koordinate des Animations-Mittelpunkts
     * @param   y   y-Koordinate des Animations-Mittelpunkts
     */
    public void kreisAnimation(Raum r, int x, int y)
    {
        am.kreisAnimation(r, new Punkt(x,y));
    }
    
    
    /**
     * Methode kreisAnimation
     * Drehrichtung ist vorgegeben
     *
     * @param   r   Das zu animierende Grafik-Objekt
     * @param   x   x-Koordinate des Animations-Mittelpunkts
     * @param   y   y-Koordinate des Animations-Mittelpunkts
     * @param   t   Umlaufzeit in Millisekunden
     */
    public void kreisAnimation(Raum r, int x, int y, int t)
    {
        am.kreisAnimation(r, new Punkt(x,y), t);
    }
    
    
    /**
     * Methode geradenAnimation
     *
     * @param   r   Das zu animierende Grafik-Objekt
     * @param   x   x-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     * @param   y   y-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     */
    public void geradenAnimation(Raum r, int x, int y)
    {
        am.geradenAnimation(r, new Punkt(x,y));
    }
    
    
    /**
     * Methode geradenAnimation
     *
     * @param   r           Das zu animierende Grafik-Objekt
     * @param   x           x-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     * @param   y           y-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     * @param   zielZeit    Die Zeit in Millisekunden, bis der Ziel-Punkt erreicht ist
     */
    public void geradenAnimation(Raum r, int x, int y, int zielZeit)
    {
        am.geradenAnimation(r, new Punkt(x,y), zielZeit);
    }
    
    
    /**
     * Methode geradenAnimation
     *
     * @param   r               Das zu animierende Grafik-Objekt
     * @param   x               x-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     * @param   y               y-Koordinate des Punkts, auf den sich das Grafik-Objekt zu bewegen soll
     * @param   zielZeit        Die Zeit in Millisekunden, bis der Ziel-Punkt erreicht ist
     * @param   animationsZeit  Die Zeitin Millisekunden, bis die Animation beendet wird
     */
    public void geradenAnimation(Raum r, int x, int y, int zielZeit, int animationsZeit)
    {
        am.geradenAnimation(r, new Punkt(x,y), zielZeit, animationsZeit);
    }
    
    
    /**
     * Methode streckenAnimation
     *
     * @param   r       Das zu animierende Grafik-Objekt
     * @param   punkte  Beliebige Anzahl von Punkt-Objekten (durch Komma getrennt).
     *                  Der erste Punkt ist automatisch immer der urspruengliche Mittelpunkt des Grafik-Objekts. Dieser Punkt darf NICHT mit genannt werden!
     *                  Punkte mit <code>new ea.Punkt(x-Koordinate, y-Koordinate)</code> erzeugen.
     */
    public void streckenAnimation(Raum r, Punkt... punkte)
    {
        am.streckenAnimation(r, punkte);
    }
    
    
    /**
     * Methode streckenAnimation
     *
     * @param   r           Das zu animierende Grafik-Objekt
     * @param   laufDauer   Die Zeit in Millisekunden, bis alle "Etappen"-Punkte einmal abgegangen wurden.
     * @param   punkte      Beliebige Anzahl von Punkt-Objekten (durch Komma getrennt).
     *                      Der erste Punkt ist automatisch immer der urspruengliche Mittelpunkt des Grafik-Objekts. Dieser Punkt darf NICHT mit genannt werden!
     *                      Punkte mit <code>new ea.Punkt(x-Koordinate, y-Koordinate)</code> erzeugen.
     */
    public void streckenAnimation(Raum r, int laufDauer, Punkt... punkte)
    {
        am.streckenAnimation(r, laufDauer, punkte);
    }
    
    
    /**
     * Methode streckenAnimation
     *
     * @param   r           Das zu animierende Grafik-Objekt
     * @param   laufDauer   Die Zeit in Millisekunden, bis alle "Etappen"-Punkte einmal abgegangen wurden.
     * @param   wiederholen Gibt an, ob diese Animation in einer "Dauerschleife" wiederholt werden soll.
     * @param   punkte      Beliebige Anzahl von Punkt-Objekten (durch Komma getrennt).
     *                      Der erste Punkt ist automatisch immer der urspruengliche Mittelpunkt des Grafik-Objekts. Dieser Punkt darf NICHT mit genannt werden!
     *                      Punkte mit <code>new ea.Punkt(x-Koordinate, y-Koordinate)</code> erzeugen.
     */
    public void streckenAnimation(Raum r, int laufDauer, boolean wiederholen, Punkt... punkte)
    {
        am.streckenAnimation(r, laufDauer, wiederholen, punkte);
    }
    
    
    /**
     * Methode streckenAnimation
     *
     * @param   r           Das zu animierende Grafik-Objekt
     * @param   laufDauer   Die Zeit in Millisekunden, bis alle "Etappen"-Punkte einmal abgegangen wurden.
     * @param   wiederholen Gibt an, ob diese Animation in einer "Dauerschleife" wiederholt werden soll.
     * @param   geschlossen Gibt an, ob bei Wiederholung die Animation vom letzten wieder zum ersten Punkt laufen soll oder ob sie vom letzten zum vorletzten (usw) rueckwaerts weiterlaufen soll. 
     *                      Dieser Parameter ist sinnlos, wenn wiederholen false ist.
     * @param   punkte      Beliebige Anzahl von Punkt-Objekten (durch Komma getrennt).
     *                      Der erste Punkt ist automatisch immer der urspruengliche Mittelpunkt des Grafik-Objekts. Dieser Punkt darf NICHT mit genannt werden!
     *                      Punkte mit <code>new ea.Punkt(x-Koordinate, y-Koordinate)</code> erzeugen.
     */
    public void streckenAnimation(Raum r, int laufDauer, boolean wiederholen, boolean geschlossen, Punkt... punkte)
    {
        am.streckenAnimation(r, laufDauer, wiederholen, geschlossen, punkte);
    }
    
    
    /**
     * Methode animationBeendenVon
     *
     * @param   r   Das Grafik-Objekt, dessen Animation gestoppt werden soll
     */
    public void animationBeendenVon(Raum r)
    {
        am.animationBeendenVon(r);
    }
    
    
    // nur zum Testen
    public void animiere( Raum r , int zeit , int strecke )
    {
        float schritt = 2;
        int delay = zeit / (strecke/2);
        int anzahl = zeit / delay;
        while(true)
        {
            for ( int z=0 ; z<anzahl ; z++ )
            {
                r.bewegen( schritt , 0 );
                try {
                    Thread.sleep(delay);
                }
                catch( InterruptedException e){}
            }
            for ( int z=0 ; z<anzahl ; z++ )
            {
                r.bewegen( -schritt , 0 );
                try {
                    Thread.sleep(delay);
                }
                catch( InterruptedException e){}
            }
        }
    }
}
