import ea.*;

/**
 * Klasse TICKMANAGER zur Realisierung einfacher nebenlaeufiger Prozesse.
 * 
 * @author      Mike Ganshorn
 * @version     v0.1 (2016-05-27)
 */
public class TICKMANAGER
{

    /**
     * Konstruktor der Klasse TICKMANAGER
     */
    public TICKMANAGER()
    {
    }

    
    /**
     * Meldet einen Ticker am Manager an. 
     * Ab sofort laeuft er auf diesem Manager und damit wird auch 
     * dessen tick()-Methode immer wieder aufgerufen.
     *
     * @param   t   Referenz auf das Ticker-Objekt
     * @param   milliSekunden   Dauer zwischen den Ticks in Milli-Sekunden
     */
    public void anmelden(TICKER t , int milliSekunden)
    {
        Manager.standard.anmelden(t , milliSekunden);
    }
    
    
    /**
     * Macht diesem Manager einen Ticker bekannt, OHNE ihn zu starten.
     *
     * @param   t   Referenz auf das Ticker-Objekt
     */
    public void anmelden(TICKER t)
    {
        Manager.standard.anmelden(t);

    }

    
    /**
     * Diese Methode setzt das Intervall eines Tickers neu.
     *
     * @param   t   Referenz auf das Ticker-Objekt
     * @param   milliSekunden   Dauer zwischen den Ticks in Milli-Sekunden
     */
    public void setzeIntervall(TICKER t , int milliSekunden)
    {
        Manager.standard.intervallSetzen(t , milliSekunden);
    }

    
    /**
     * Haelt einen Ticker an, der bereits am TICKMANAGER angemeldet ist. 
     * Ist der Ticker bereits angehalten, passiert gar nichts.
     *
     * @param   t   Der Referenz auf das anzuhaltenden Ticker-Objekt
     */
    public void anhalten(TICKER t)
    {
        Manager.standard.anhalten(t);
    }

    
    /**
     * Startet einen Ticker, der bereits an diesem Manager angemeldet ist.
     * Laeuft der Ticker bereits, passiert gar nichts. 
     * War der Ticker nicht angemeldet, kommt eine Fehlermeldung.
     *
     * @param   t   Referenz auf das Ticker-Objekt
     * @param   milliSekunden   Dauer zwischen den ticks in Milli-Sekunden
     */
    public void starten(TICKER t, int milliSekunden)
    {
        Manager.standard.starten(t , milliSekunden);
    }

}
