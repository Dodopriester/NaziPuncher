/** @author     mike ganshorn + michael andonie
 * 
 *  @Version    2.4 (2016-02-17)
 *  
 *  @changelog  2.4 WECHSELBILD erbt von Knoten und damit von Raum
 *                  verschiebenUm greift auf bewegen zurueck
 *                  Methoden in allen Klassen vereinheitlicht (bis auf indiv. Methoden)
 *  
 *              2.3 Methode beruehrt(WECHSELBILD) hinzugefuegt
 *  
 *              2.2 Jump'n'Run-Physik hinzu gefuegt
 *  
 *              2.1 Bei Aenderung der Breite/Hoehe bleibt der Mittelpunkt erhalten
 *                  Keine Abhaengigkeit mehr zwischen den alpha-Formen
 *              
*/

import ea.edu.RechteckE;
import ea.Raum;

/**
 * Diese Klasse stellt ein einfaches Rechteck dar.
 */
public class RECHTECK 
        extends RechteckE 
{
        
    /**
     * Die Farbe dieses Rechtecks
     */
    private String farbe;
    
    /**
     * Gibt an, ob dieses Rechteck sichtbar ist.
     */
    private boolean sichtbar;
    
    /**
     * Breite des Rechtecks
     */
    private int breite;
    
    /**
     * Hoehe des Rechtecks
     */
    private int hoehe;
    
    /**
     * x-Koordinate ds Mittelpunkts
     */
    private int M_x;
    
    /**
     * y-Koordinate des Mittelpunkts
     */
    private int M_y;
    
    
    /**
     * Konstruktor der Klasse <code>RECHTECK</code>. Erstellt ein neues Standard-Rechteck.
     */
    public RECHTECK() 
    {
        this(200, 130);
    }
    
    
    /**
     * Konstruktor der Klasse <code>RECHTECK</code>. Erstellt ein neues Rechteck mit gegebenen Massen.
     *
     * @param   breite  Breite des Rechtecks
     * @param   hoehe   Hoehe des Rechtecks
     */
    public RECHTECK(int breite, int hoehe) 
    {
        this.sichtbar = true;
        super.sichtbarSetzen(true);
        this.farbe = "Rot";
        super.farbeSetzen(this.farbe);
        this.breite = breite;
        this.hoehe = hoehe;
        super.masseSetzen(this.breite, this.hoehe);
        this.M_x = 250;
        this.M_y = 250;
        super.mittelpunktSetzen(this.M_x, this.M_y);
    }
    
    
    /**
     * Setzt die Hoehe und Breite dieses Rechtecks neu.
     * @param   breite  Die neue Breite dieses Rechtecks
     * @param   hoehe   Die neue Hoehe dieses Rechtecks
     */
    public void setzeGroesse(int breite, int hoehe) 
    {
        int x = this.nenneM_x();
        int y = this.nenneM_y();
        this.breite = breite;
        this.hoehe = hoehe;
        super.masseSetzen(breite, hoehe);
        super.mittelpunktSetzen(x, y);
    }
    
    
    /**
     * Setzt die Farbe dieses Rechtecks neu.
     * @param   farbeNeu    Diese Farbe erhaelt das Rechteck (z.B. "Rot")
     */
    public void setzeFarbe(String farbeNeu) 
    {
        this.farbe = farbeNeu;
        super.farbeSetzen(farbe);
    }
    
    
    /**
     * Setzt den Mittelpunkt dieses Rechtecks neu.
     * @param   m_x Die X-Koordinate des neuen Mittelpunktes
     * @param   m_y Die Y-Koordinate des neuen Mittelpunktes
     */
    public void setzeMittelpunkt(int m_x, int m_y) 
    {
        this.M_x = m_x;
        this.M_y = m_y;
        super.mittelpunktSetzen(m_x, m_y);
    }
    
    
    /**
     * Setzt, ob dieses Rechteck sichtbar sein soll.
     * @param   sichtbarNeu Ist dieser Wert <code>true</code>, ist nach dem Aufruf dieser Methode dieses Rechteck 
     * sichtbar. Ist dieser Wert <code>false</code>, so ist nach dem Aufruf dieser Methode dieses Rechteck unsichtbar.
     */
    public void setzeSichtbar(boolean sichtbarNeu) 
    {
        this.sichtbar = sichtbarNeu;
        super.sichtbarSetzen(sichtbarNeu);
    }
    
    
    /**
     * Verschiebt dieses Rechteck um eine Verschiebung - angegeben durch ein "Delta X" und "Delta Y".
     * @param   deltaX  Der X Anteil dieser Verschiebung. Positive Werte verschieben nach rechts, negative nach links.
     * @param   deltaY  Der Y Anteil dieser Verschiebung. Positive Werte verschieben nach unten, negative nach oben.
     */
    public void verschiebenUm(int deltaX, int deltaY) 
    {
        this.M_x = this.M_x + deltaX;
        this.M_y = this.M_y + deltaY;
        super.bewegen(deltaX, deltaY);
    }
    
    
    /**
     * Methode beruehrt
     *
     * @param   r   Ein anderes BILD, RECHTECK, KREIS, DREIECK, ...
     * @return  true, wenn sich die beiden Objekte ueberschneiden
     */
    public boolean beruehrt(Raum r) 
    {
        return super.schneidet(r);
    }
    
    
    /**
     * Prueft, ob dieses Grafik-Objekt einen Punkt beruehrt. 
     *
     * @param   p   Der Punkt
     * @return      'true', wenn der Punkt beruehrt wird, sonst 'false'
     */
    public boolean beruehrt(ea.Punkt p)
    {
        return super.beinhaltet(p);
    }
    
    
    /**
     * Methode beinhaltetPunkt
     *
     * @param   x   x-Koordinate des Punkts (Pixel)
     * @param   y   x-Koordinate des Punkts (Pixel)
     * @return      true, wenn Punkt innerhalb der Grafik
     */
    public boolean beinhaltetPunkt(int x, int y) 
    {
        return super.beinhaltet( new ea.Punkt(x, y) );
    }
    
       
    /**
     * Diese Methode gibt die x-Koordinate des Mittelpunkts dieses Rechtecks zurueck
     * @return  Die aktuelle x-Koordinate des Mittelpunktes dieses Rechtecks
     */
    public int nenneM_x()
    {
        return this.M_x;
    }
    
    
    /**
     * Diese Methode gibt die y-Koordinate des Mittelpunkts dieses Rechtecks zurueck
     * @return  Die aktuelle y-Koordinate des Mittelpunktes dieses Rechtecks
     */
    public int nenneM_y()
    {
        return this.M_y;
    }
    
    
    /**
     * Diese Methode gibt die Breite dieses Rechtecks zurueck
     * @return  Die aktuelle Breite dieses Rechtecks
     */
    public int nenneBreite()
    {
        return this.breite;
    }
    
    
    /**
     * Diese Methode gibt die Hoehe dieses Rechtecks zurueck
     * @return  Die aktuelle Hoehe dieses Rechtecks
     */
    public int nenneHoehe()
    {
        return this.hoehe;
    }
    
    
    /**
     * Diese Methode gibt die Farbe dieses Rechtecks zurueck
     * @return  Die aktuelle Farbe dieses Rechtecks
     */
    public String nenneFarbe()
    {
        return this.farbe;
    }
    
    
    /**
     * Diese Methode gibt die Sichtbarkeit dieses Rechtecks zurueck
     * @return  Die aktuelle Sichtbarkeit dieses Rechtecks
     */
    public boolean nenneSichtbar()
    {
        return this.sichtbar;
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Rechtecks vom Mittelpunkt 
     * eines anderen Grfik-Objekts in x-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Rechtecks vom anderen Grafik-Objekt in x-Richtung (>0, wenn dieses Rechteck rechts des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandX(Raum grafikObjekt)
    {
        return this.M_x - grafikObjekt.mittelPunkt().x();
    }
    
    
    /**
     * Diese Methode prueft, wie weit der Mittelpunkt dieses Rechtecks vom Mittelpunkt 
     * eines anderen Grfik-Objekts in y-Richtung entfernt ist.
     * @param   grafikObjekt    Das andere Grafik-Objekt
     * @return  Abstand (in Pixeln) dieses Rechtecks vom anderen Grafik-Objekt in y-Richtung (>0, wenn dieses Rechteck unterhalb des anderen Grafik-Objekts liegt)
     */
    public int berechneAbstandY(Raum grafikObjekt)
    {
        return this.M_y - grafikObjekt.mittelPunkt().y();
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun aktiv und kann passive Objekte NICHT durchdringen.
     */
    @Override
    public void aktivMachen()
    {
        super.aktivMachen();
    }
    
    
    /**
     * Methode zum Deaktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     */
    @Override
    public void neutralMachen()
    {
        super.neutralMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void passivMachen()
    {
        super.passivMachen();
    }
    
    
    /**
     * Methode zum Aktivieren der Schwerkraft fuer dieses Objekt. 
     * Von nun an unterliegt das Objekt der Schwerkraft und faellt runter.
     * 
     * @param   b   'true', wenn die Schwerkraft wirken soll, 'false', wenn nicht.
     */
    @Override
    public void schwerkraftAktivSetzen(boolean b)
    {
        super.schwerkraftAktivSetzen(b);
    }
    
    
    /**
     * Methode zum Aktivieren der Jump'n'Run-Physik fuer dieses Objekt. 
     * Dieses Objekt ist nun passiv und kann aktive Objekte "wegschieben". 
     * Ausserdem landen aktive Objekte auf passiven Objekten, 
     * wenn die Schwerkraft des aktiven Objekts aktiv ist.
     */
    @Override
    public void schwerkraftSetzen(int n)
    {
        super.schwerkraftSetzen(n);
    }
    
    
    /**
     * Methode zum hochspringen, wenn das Objekt aktiv ist und auf einem passiven Objekt steht.
     *
     * @param   staerke     1 = schwach ... 10 = stark
     * @return  Der Rückgabewert
     */
    @Override
    public boolean sprung(int staerke)
    {
        return super.sprung(staerke);
    }
    
    
    /**
     * Prueft ob dieses Grafik-Objekt, wenn es aktiv ist, auf einem anderen, passiven Grafik-Objekt steht.
     *
     * @param   r   Das andere Grafik-Objekt
     * @return  'true', wenn dieses Grafik-Objekt aktiv gesetzt ist und auf einem passiven Grafik-Objekt steht
     */
    @Override
    public boolean stehtAuf(Raum r)
    {
        return super.stehtAuf(r);
    }
    
    
    /**
     * Gibt an, ob dieses (aktive) Grafik-Objekt auf irgend einem anderen (passiven) Grafik-Objekt steht. 
     * Gibt nur Sinn, wenn dieses Objekt aktiv ist!
     *
     * @return  'true', wenn dieses Objekt auf irgend einem anderen steht; 'false', wenn nicht
     */
    public boolean steht()
    {
        return super.steht();
    }
    
    
    /**
     * liefert den Sinus des Drehwinkels der Grafik
     *
     * @return  Sinus des aktuellen Drehwinkels
     */
    public double sin_Drehwinkel()
    {
        return Math.sin( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * liefert den Cosinus des Drehwinkels der Grafik
     *
     * @return  Cosinus des aktuellen Drehwinkels
     */
    public double cos_Drehwinkel()
    {
        return Math.cos( this.gibDrehung() * Math.PI / 180 );
    }
    
    
    /**
     * Dreht die Grafik um einen Winkel
     *
     * @param   winkelAenderung     +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void drehenUm(int winkelAenderung)
    {
        this.drehenRelativ( (double)( -winkelAenderung ) );
    }
    
    
    /**
     * Setzt den Drehwinkel auf eine absoluten neuen Wert
     *
     * @param   neuerDrehwinkel     der neue Drehwinkel
     *                              +: mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *                              -: mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public void setzeDrehwinkel(int neuerDrehwinkel)
    {
        this.drehenAbsolut( (double)( -neuerDrehwinkel ) );
    }
    
    
    /**
     * Nennt den Winkel, um den die Grafik gedreht wurde
     *
     * @return      der Winkel, um den die Grafik gedreht wurde
     *              0: wenn nicht gedreht
     *              +: wenn mathematisch positiver Drehsinn (gegen den Uhrzeigersinn)
     *              -: wenn mathematisch negativer Drehsinn (im Uhrzeigersinn)
     */
    public int nenneWinkel()
    {
        return (int)( -this.gibDrehung() );
    }
    
    
//     /**
//      * Prueft, ob ein WECHSELBILD-Objekt beruehrt wird.
//      *
//      * @param   wb  Das WECHSELBILD-Objekt
//      * @return  'true', wenn ein anderes WECHSELBILD-Objekt beruehrt wird
//      */
//     public boolean beruehrt(WECHSELBILD wb)
//     {
//         return wb.beruehrt(this);
//     }
    
}