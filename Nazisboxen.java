public class Nazisboxen extends SPIEL
{
    private RECHTECK Boden;
    private FIGUR Kaenguru;
    private FIGUR Nazi;
    private String modus;
    
    public Nazisboxen()
    {
        super();
        Boden = new RECHTECK(850, 50);
        Boden.setzeMittelpunkt(400, 500);
        Boden.setzeFarbe("weiss");
        Boden.passivMachen();
        
        Kaenguru = new FIGUR(50, 450, "KaenguruNormal.eaf");
        Kaenguru.aktivMachen();
        
        Nazi = new FIGUR(500, 450, "Nazi.eaf");
        Nazi.aktivMachen();
        
        modus = "stehen";
        
        super.tickerNeuStarten(10);
        
    }
    
    @Override
    public void aufrufen()
    {
        if(modus == "rgehen")
        {
            Kaenguru.verschiebenUm(1, 0);
        } else if(modus == "lgehen")
        {
            Kaenguru.verschiebenUm(-1, 0);
        }
    }
    
    @Override
    public void tasteReagieren(int tastenkuerzel)
    {
        if(tastenkuerzel == 3)
        {
            modus = "rgehen";
            Kaenguru.spiegelXSetzen(false);
        } else if(tastenkuerzel == 0)
        {
            modus = "lgehen";
            Kaenguru.spiegelXSetzen(true);
        } else if(tastenkuerzel == 18)
        {
            modus = "stehen";
        } else if(tastenkuerzel == 30)
        {
            Kaenguru.sprung(7);
        }
    }
}